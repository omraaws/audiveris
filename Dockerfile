FROM openjdk:8
MAINTAINER Maciej Gamrat

RUN apt-get update -qq && \
    apt-get install -qq ant make tesseract-ocr tesseract-ocr-dev && \
    rm -rf /var/lib/apt/lists/*

VOLUME ["/data"]

ADD . /src/audiveris
RUN cd /src/audiveris && \
    make clean && \
    make all && \
    make install && \
    rm -rf /src/audiveris

WORKDIR /data
ENTRYPOINT ["/usr/local/bin/audiveris"]
CMD ["-help"]
