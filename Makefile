DOCKER = docker

all: jar

jar: dist/audiveris.jar

dist/audiveris.jar:
	ant jar

clean:
	ant clean

install:
	# copy compiled project files
	cp -R dist /opt/audiveris

	# copy project wrapper
	cp -R bin /opt/audiveris/bin

	# make wrapper executable with `audiveris` command by linking to /usr/local/bin
	ln -sf /opt/audiveris/bin/audiveris /usr/local/bin/audiveris

uninstall:
	# remove wrapper script from /usr/local/bin
	[ -L /usr/local/bin/audiveris ] && rm /usr/local/bin/audiveris || true

	# remove project installation
	rm -rf /opt/audiveris

build:
	$(DOCKER) build -t omraaws/audiveris:latest .

rmimage:
	$(DOCKER) rmi omraaws/audiveris || true

.PHONY: all jar clean install uninstall build rmimage
